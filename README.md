# README #

Execution Steps:

1. Download the project.

2. Download OpenCV 3.2.0 from [here](https://sourceforge.net/projects/opencvlibrary/files/) and link it to the project.


3. Compile and Run the ExecuteMe.java class present in the path VirtualRuler_CV_Spring17\src\com\java\Test\ExecuteMe.java

Results:

1. Reference Object Method result 'ReferenceResults.JPG' can be found in the path VirtualRuler_CV_Spring17\Resources\Images\ReferenceResults.JPG

2. Stereo Object Method result can be found in the path VirtualRuler_CV_Spring17\Resources\Images\StereoResults.JPG